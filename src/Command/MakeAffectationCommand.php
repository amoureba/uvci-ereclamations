<?php

namespace App\Command;

use App\Entity\Registration;
use App\Repository\LevelRepository;
use App\Repository\RegistrationRepository;
use App\Repository\SpecialtyRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MakeAffectationCommand extends Command
{
    protected static $defaultName = 'make:affectations';
    private $userRepository;
    private $manager;
    private $levelRepository;
    private $specialtyRepository;
    private $registrationRepository;

    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $manager,
        LevelRepository $levelRepository,
        SpecialtyRepository $specialtyRepository,
        RegistrationRepository $registrationRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->manager = $manager;
        $this->levelRepository = $levelRepository;
        $this->specialtyRepository = $specialtyRepository;
        $this->registrationRepository = $registrationRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $registrations_json = file_get_contents("https://scolarite.uvci.edu.ci/ApiReclamation/get_inscription_semestrielle_students");
        $registrations = json_decode($registrations_json, true);
        if ($registrations["success"])
        {
            $registrations = $registrations["inscription"];
            $progressBar = new ProgressBar($output, sizeof($registrations));
            $progressBar->start();
            foreach ($registrations as $registration)
            {
                $user = $this->userRepository->findOneBy(["email" => $registration["email"]]);
                if (!is_null($user))
                {
                    $level = $this->levelRepository->findOneBy(["wording" => $registration["niveau"]]);
                    $specialty = $this->specialtyRepository->findOneBy(["coded" => $registration["specialite"]]);

                    if (!is_null($level) && !is_null($specialty))
                    {
                        $inscription = $this->registrationRepository->findOneBy([
                            "level" => $level,
                            "specialty" => $specialty,
                            "semestre" => $registration["semestre"],
                            "rentree" => $registration["rentree"],
                        ]);

                        // Si l'inscription existe déjà
                        if (is_null($inscription))
                        {
                            $inscription = new Registration();
                            $inscription->setUser($user);
                            $inscription->setLevel($level);
                            $inscription->setSpecialty($specialty);
                            $inscription->setRentree($registration["rentree"]);
                            $inscription->setSemestre($registration["semestre"]);

                            $this->manager->persist($inscription);
                            $progressBar->advance();
                        }
                    }
                }
            }
            $this->manager->flush();
            $progressBar->finish();
            $io->success("Les affectations ont été effectuées avec succès !");
        }
        else
        {
            $io->error("Une erreur s'est produite lors de l'affectation des étudiants. Merci de réessayer plus tard !");
        }

        return Command::SUCCESS;
    }
}
