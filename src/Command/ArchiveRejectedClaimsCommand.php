<?php

namespace App\Command;

use App\Repository\ClaimRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ArchiveRejectedClaimsCommand extends Command
{
    protected static $defaultName = 'archive:rejected:claims';
    private $claimsRepository;
    private $manager;

    public function __construct(ClaimRepository $claimRepository, EntityManagerInterface $manager)
    {
        $this->claimsRepository = $claimRepository;
        $this->manager = $manager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Archive rejected claims command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $claims = $this->claimsRepository->findBy([
            'rejected' => true,
            'archived' => false,
        ]);
        $progressBar = new ProgressBar($output, sizeof($claims));
        foreach ($claims as $claim) {
            $claim->setArchived(true);
            $this->manager->persist($claim);
            $progressBar->advance();
        }
        $progressBar->finish();

        $io->success('Opération terminée avec succès !');

        return Command::SUCCESS;
    }
}
