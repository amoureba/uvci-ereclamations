<?php

namespace App\Command;

use App\Repository\ClaimRepository;
use App\Repository\ExaminationRepository;
use App\Repository\ExamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class SendEmailTreatedStudentsCommand extends Command
{
    protected static $defaultName = 'send:email:treated:claims:students';
    private $examRepository;
    private $examinationRepository;
    private $claimsRepository;
    private $mailer;
    private $manager;

    public function __construct(ExamRepository $examRepository,
                                ExaminationRepository $examinationRepository,
                                ClaimRepository $claimRepository, MailerInterface $mailer,
                                EntityManagerInterface $manager
    )
    {
        $this->examRepository = $examRepository;
        $this->examinationRepository = $examinationRepository;
        $this->claimsRepository = $claimRepository;
        $this->mailer = $mailer;
        $this->manager = $manager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Send emails to students')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $exams = $this->examRepository->findBy([
            'archived' => false,
        ]);
        $examinations = $this->examinationRepository->findBy(['exam' => $exams]);
        $claims = $this->claimsRepository->findBy(['examination' => $examinations]);
        $progressBar = new ProgressBar($output, sizeof($claims));
        $from = 'noreply.reclamations@uvci.edu.ci';
        $fromName = "Réclamation";
        foreach ($claims as $key => $claim) {
            $author = $claim->getAuthor();
            $student_name = $author->getFullName();
            $examination = $claim->getExamination();
            $examinationMatter = $examination->getMatterSpecialty()->getMatiere();
            $examSemester = $examination->getExam()->getSemester();
            $examSession = $examination->getExam()->getSession();
            $to = $claim->getAuthor()->getEmail();
            $level = $examination->getExam()->getLevel()->getWording();
            $course_url = "https://licence".strtolower($examSemester).".uvci.edu.ci/";
            if (strpos($level, 'MASTER') !== false) {
                $course_url = "https://master.uvci.edu.ci/";
            }
            $email = (new TemplatedEmail())
                ->from(new Address($from, $fromName))
                ->to($to)
                ->subject("Reclamation traitée")
                ->htmlTemplate('emails/copies.html.twig')
                ->context([
                    'claim_wording' => $claim->getWording(),
                    'claim_created_at' => $claim->getCreatedAt(),
                    'examination_matter' => $examinationMatter,
                    'exam_semester' => $examSemester,
                    'exam_session' => $examSession,
                    'student_name' => $student_name,
                    'course_url' => $course_url,
                    'level' => $level,
                ]);
            try {
                $this->mailer->send($email);
            } catch (TransportExceptionInterface $e) {
            }
            if ($key == 1) {
                $email = (new TemplatedEmail())
                    ->from(new Address($from, $fromName))
                    ->to('pedagogie@uvci.edu.ci')
                    ->subject("Reclamation traitée")
                    ->htmlTemplate('emails/copies.html.twig')
                    ->context([
                        'claim_wording' => $claim->getWording(),
                        'claim_created_at' => $claim->getCreatedAt(),
                        'examination_matter' => $examinationMatter,
                        'exam_semester' => $examSemester,
                        'exam_session' => $examSession,
                        'student_name' => $student_name,
                        'course_url' => $course_url,
                        'level' => 'Peda',
                    ]);
                try {
                    $this->mailer->send($email);
                } catch (TransportExceptionInterface $e) {
                }
            }
            $claim->setArchived(true);
            $claim->setTreated(true);
            $this->manager->persist($claim);
            $progressBar->advance();
        }
        $this->manager->flush();
        $progressBar->finish();
        $io->success('Les emails ont été bien envoyés !');

        return Command::SUCCESS;
    }
}
