<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RefreshStudentsCommand extends Command
{
    protected static $defaultName = 'refresh:students';
    private $userRepository;
    private $manager;
    private $encoder;

    public function __construct(UserRepository $userRepository,
                                UserPasswordEncoderInterface $encoder,
                                EntityManagerInterface $manager)
    {
        $this->userRepository = $userRepository;
        $this->manager = $manager;
        $this->encoder = $encoder;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Refresh students list')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $students_json = file_get_contents("https://scolarite.uvci.edu.ci/ApiReclamation/get_students");
        $students = json_decode($students_json, true);
        if ($students["success"])
        {
            $students = $students["etudiants"];
            $progressBar = new ProgressBar($output, sizeof($students));
            foreach ($students as $student)
            {
                $user = $this->userRepository->findOneBy(["email" => $student["email_uvci"]]);
                if (is_null($user))
                {
                    $user = new User();
                    $user->setFirstName($student["nom"]);
                    $user->setLastName($student["prenoms"]);
                    $user->setEmail($student["email_uvci"]);
                    $user->setProfile('ETUDIANT');
                }
                $user->setPassword($this->encoder->encodePassword($user, $student["password"]));
                $this->manager->persist($user);
                $progressBar->advance();
            }
            $this->manager->flush();
            $progressBar->finish();
            $io->success('Liste actualiser avec succès !');
        }
        else
        {
            $io->error("Une erreur s'est produite lors de l'actualisation de la liste des étudiants. Merci de réessayer plus tard !");
        }

        return Command::SUCCESS;
    }
}
