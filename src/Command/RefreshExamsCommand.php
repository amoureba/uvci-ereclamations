<?php

namespace App\Command;

use App\Repository\ExamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\String\Slugger\SluggerInterface;

class RefreshExamsCommand extends Command
{
    protected static $defaultName = 'refresh:exams';
    protected static $defaultDescription = 'Refresh exams';
    private $examRepository;
    private $slugger;
    private $manager;

    public function __construct(ExamRepository $examRepository, SluggerInterface $slugger, EntityManagerInterface $manager)
    {
        $this->examRepository = $examRepository;
        $this->slugger = $slugger;
        $this->manager = $manager;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $exams = $this->examRepository->findAll();
        $progressBar = new ProgressBar($output, sizeof($exams));
        foreach ($exams as $exam) {
            $slug = $this->slugger->slug($exam->getSession().'-'.$exam->getAcademyEntry().'-'.$exam->getLevel()->getWording().'-'.$exam->getSemester());
            $exam->setSlug($slug);
            $this->manager->persist($exam);
            $progressBar->advance();
        }
        $this->manager->flush();
        $progressBar->finish();
        $io->success('Mise à jour des slugs des examens terminée avec succès !');

        return Command::SUCCESS;
    }
}
