<?php

namespace App\Form\Admin;

use App\Entity\Exam;
use App\Entity\Level;
use App\Entity\Specialty;
use App\Form\Admin\Type\SemestersType;
use App\Form\Admin\Type\SessionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        dd($options["academyEntries"]);
        $builder
            ->add('session', SessionType::class, [
                'label' => 'Session',
                'required' => true,
                'attr' => ['class' => 'custom-select'],
            ])
            ->add("academyYear", ChoiceType::class, [
                "choices" => $options["academyYears"],
                'attr' => ['class' => 'custom-select'],
                'label' => 'Année universitaire',
            ])
            ->add("academyEntry", ChoiceType::class, [
                "choices" => $options["academyEntries"],
                'attr' => ['class' => 'custom-select'],
                'label' => 'Rentrée',
            ])
            ->add("startDate", DateType::class, [
                'widget' => 'single_text',
                "label" => "Date debut",
                'help' => 'Ex: 01/01/2020',
            ])
            ->add("endDate", DateType::class, [
                'widget' => 'single_text',
                "label" => "Date fin",
                'help' => 'Ex: 01/01/2020',
            ])
            ->add('level', EntityType::class, [
                'class' => Level::class,
                'label' => 'Niveau',
                'required' => true,
                'attr' => ['class' => 'custom-select'],
            ])
            ->add('semester', SemestersType::class, [
                'label' => 'Semestre',
                'attr' => ['class' => 'custom-select'],
            ])
            ->add('specialities', EntityType::class, [
                'class' => Specialty::class,
                'multiple' => true,
                'expanded' => true,
                'label' => 'Spécialité',
                'required' => true,
//                'attr' => ['class' => 'custom-select'],
            ])
            /*->add("matters", EntityType::class, [
                "class" => MatterSpecialty::class,
                'multiple' => true,
                'expanded' => true,
                'label' => 'Matières',
                'required' => true,
            ])*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Exam::class,
            "academyYears" => null,
            "academyEntries" => null,
        ]);
    }
}
