<?php

namespace App\Form\Admin\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SemestersType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => [
            'SEMESTRE 1' => 'S1',
            'SEMESTRE 2' => 'S2',
            'SEMESTRE 3' => 'S3',
            'SEMESTRE 4' => 'S4',
            'SEMESTRE 5' => 'S5',
            'SEMESTRE 6' => 'S6',
            ],
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}