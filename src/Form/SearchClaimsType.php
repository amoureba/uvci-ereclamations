<?php

namespace App\Form;

use App\Entity\Level;
use App\Entity\Specialty;
use App\Entity\User;
use App\Form\Admin\Type\SemestersType;
use App\Form\Admin\Type\SessionType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchClaimsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('session', SessionType::class, [
                'label' => 'Session',
                'attr' => ['class' => 'custom-select'],
                'required' => false,
            ])
            ->add('academyEntry', ChoiceType::class, [
                "choices" => $options["academyEntries"],
                'attr' => ['class' => 'custom-select'],
                'label' => 'Rentrée',
                'required' => false,
            ])
            ->add('level', EntityType::class, [
                'class' => Level::class,
                'label' => 'Niveau',
                'required' => false,
                'attr' => ['class' => 'custom-select'],
            ])
            ->add('semester', SemestersType::class, [
                'label' => 'Semestre',
                'attr' => ['class' => 'custom-select'],
                'required' => false,
            ])
            ->add('speciality', EntityType::class, [
                'class' => Specialty::class,
                'label' => 'Spécialité',
                'attr' => ['class' => 'custom-select'],
                'required' => false,
            ])
           /* ->add('email', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('u')
                        ->where('u.profile = :val')
                        ->setParameter('val', 'ETUDIANT')
                        ;
                },
                'choice_label' => 'email',
            ])*/
           ->add('email', EmailType::class, [
               'required' => false,
           ])
            ->add("startDate", DateType::class, [
                'widget' => 'single_text',
                "label" => "Date debut",
                'help' => 'Ex: 01/01/2020',
                'required' => false,
            ])
            ->add("endDate", DateType::class, [
                'widget' => 'single_text',
                "label" => "Date fin",
                'help' => 'Ex: 01/01/2020',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            "academyEntries" => null,
        ]);
    }
}
