<?php

namespace App\Form;

use App\Entity\Level;
use App\Entity\Specialty;
use App\Form\Admin\Type\SemestersType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResultsReportingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('session', ChoiceType::class, [
                'choices' => [
                    "Normale" => "normale",
                    "Rattrapage" => "rattrapage",
                ],
                'required' => true,
                'attr' => ['class' => 'custom-select'],
            ])
            ->add("academyYear", ChoiceType::class, [
                "choices" => $options["academyYears"],
                'attr' => ['class' => 'custom-select'],
                'label' => 'Année universitaire',
            ])
            ->add('academyEntry', ChoiceType::class, [
                "choices" => $options["academyEntries"],
                'attr' => ['class' => 'custom-select'],
                'label' => 'Rentrée',
                'required' => true,
            ])
            ->add('level', EntityType::class, [
                'class' => Level::class,
                'label' => 'Niveau',
                'required' => true,
                'attr' => ['class' => 'custom-select'],
            ])
            ->add('semester', SemestersType::class, [
                'label' => 'Semestre',
                'attr' => ['class' => 'custom-select'],
                'required' => true,
            ])
            ->add("startDate", DateType::class, [
                'widget' => 'single_text',
                "label" => "Date debut",
                'help' => 'Ex: 01/01/2020',
                'required' => true,
            ])
            ->add("endDate", DateType::class, [
                'widget' => 'single_text',
                "label" => "Date fin",
                'help' => 'Ex: 01/01/2020',
                'required' => true,
            ])
            ->add('notabene', TextareaType::class, [
                'required' => false,
                'label' => "Nota Bene",
            ])
            ->add('specialities', EntityType::class, [
                'class' => Specialty::class,
                'multiple' => true,
                'expanded' => true,
                'label' => 'Spécialité',
                'required' => true,
            ])
            ->add("preview", SubmitType::class, [
                "label" => "Prévisualiser",
                "attr" => ["class" => "btn btn-success col-md-4", "formtarget" => "_blank"]
            ])
            ->add("send", SubmitType::class, [
                "label" => "Envoyer",
                "attr" => ["class" => "btn btn-warning col-md-4"]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            "academyYears" => null,
            "academyEntries" => null,
        ]);
    }
}
