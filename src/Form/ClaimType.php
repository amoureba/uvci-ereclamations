<?php

namespace App\Form;

use App\Entity\Claim;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ClaimType extends AbstractType
{

    // doc upload file : https://symfony.com/doc/current/controller/upload_file.html

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('evaluation', ChoiceType::class, [
                'choices' => [
                    "Devoir 1" => "Devoir 1",
                    "Devoir 2" => "Devoir 2",
                    "Devoir 3" => "Devoir 3",
                    "Devoir 4" => "Devoir 4",
                    "Devoir 5" => "Devoir 5",
                    "Devoir 6" => "Devoir 6",
                    "Devoir 7" => "Devoir 7",
                    "Devoir 8" => "Devoir 8",
                    "Devoir 9" => "Devoir 9",
                    "Devoir 10" => "Devoir 10",
                ],
                'label' => "Devoir",
                'required' => true,
                'attr' => ['class' => 'custom-select'],
            ])
            ->add('wording', ChoiceType::class, [
                'choices' => [
                    "Erreur sur mon identité" => "1",
                    "Non d'affichage de ma note" => "2",
                    "Réclamation sur la moyenne de l'ECUE" => "3",
                    "Réclamation sur la correction de ma copie" => "4",
                    "Autre" => "5",
                ],
                'label' => 'Objet',
                'required' => true,
                'attr' => ['class' => 'custom-select'],
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Message'
            ])
            ->add('capture', FileType::class, [
            'label' => 'Capture',
            'required' => true,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png'
                    ],
                    'mimeTypesMessage' => 'Choisissez une image (png, jpg, jpeg) et réessayez !',
                ])
            ],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Claim::class,
        ]);
    }
}
