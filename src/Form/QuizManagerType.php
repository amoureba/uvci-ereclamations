<?php

namespace App\Form;

use App\Entity\Level;
use App\Entity\Specialty;
use App\Form\Admin\Type\SemestersType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuizManagerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("academyYear", ChoiceType::class, [
                "choices" => $options["academyYears"],
                'attr' => ['class' => 'custom-select'],
                'label' => 'Année universitaire',
            ])
            ->add('academyEntry', ChoiceType::class, [
                "choices" => $options["academyEntries"],
                'attr' => ['class' => 'custom-select'],
                'label' => 'Rentrée',
                'required' => true,
            ])
            ->add('level', EntityType::class, [
                'class' => Level::class,
                'label' => 'Niveau',
                'required' => true,
                'attr' => ['class' => 'custom-select'],
            ])
            ->add('semester', SemestersType::class, [
                'label' => 'Semestre',
                'attr' => ['class' => 'custom-select'],
                'required' => true,
            ])
            ->add('specialities', EntityType::class, [
                'class' => Specialty::class,
                'multiple' => true,
                'expanded' => true,
                'label' => 'Spécialité',
                'required' => true,
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Activez les réclamations sur les devoirs',
                'required' => false,
                'help' => "Sélectionnez pour activer les réclamations sur les devoirs ou désélectionnez pour désactiver",
            ])
            ->add("send", SubmitType::class, [
                "label" => "Envoyer",
                "attr" => ["class" => "btn btn-warning col-md-4"]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            "academyYears" => null,
            "academyEntries" => null,
        ]);
    }
}
