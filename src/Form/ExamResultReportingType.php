<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExamResultReportingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('notabene', TextareaType::class, [
                'required' => false,
                'label' => "Nota Bene",
            ])
            ->add("preview", SubmitType::class, [
                "label" => "Prévisualiser",
                "attr" => ["class" => "btn btn-success col-md-4", "formtarget" => "_blank"]
            ])
            ->add("send", SubmitType::class, [
                "label" => "Envoyer",
                "attr" => ["class" => "btn btn-warning col-md-4"]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
