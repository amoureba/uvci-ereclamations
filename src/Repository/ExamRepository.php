<?php

namespace App\Repository;

use App\Entity\Exam;
use App\Entity\Level;
use App\Entity\Specialty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Exam|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exam|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exam[]    findAll()
 * @method Exam[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exam::class);
    }

    // /**
    //  * @return Exam[] Returns an array of Exam objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Exam
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findExam(Specialty $specialty, string $semester, Level $level, string $entry)
    {
        $qb = $this->createQueryBuilder("e")
            ->where(":speciality 
            MEMBER OF e.specialities 
            AND :semester = e.semester 
            AND :level = e.level
            AND :entry = e.academyEntry
            ")
            ->setParameters([
                "speciality" => $specialty,
                "semester" => $semester,
                "level" => $level,
                "entry" => $entry,
            ]);

        return $qb->getQuery()->getResult();
    }
}
