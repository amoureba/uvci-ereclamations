<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\ClaimRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\FormLoginAuthenticator;

class ApiLoginController extends AbstractController
{
    /**
     * @Route("/api/login", name="api_login")
     */
    public function index(
        Request $request, UserRepository $userRepository,
        UserAuthenticatorInterface $userAuthenticator,
        FormLoginAuthenticator $formLoginAuthenticator
    ): Response
    {
        dd($this->getUser());
        if (null === $this->getUser()) {
            $username = $request->get("username");
            $user = $userRepository->findOneBy(["email" => $username]);
            if ($user) {
                return $userAuthenticator->authenticateUser(
                    $user,
                    $formLoginAuthenticator,
                    $request
                );
            }
            dd("Non");
            return $this->json([
                "error" => true,
                "message" => "Désolé, veuillez aller sur reclamation.uvci.edu.ci. Une erreur est survenue pour le moment. Merci pour votre compréhension !",
            ], Response::HTTP_UNAUTHORIZED);
        }

//        header('Location: ereclamation/public/compte/accueil');


        return $this->json([
            "error" => false
        ]);
    }
}
