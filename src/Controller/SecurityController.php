<?php

namespace App\Controller;

use App\Entity\Registration;
use App\Entity\User;
use App\Repository\LevelRepository;
use App\Repository\RegistrationRepository;
use App\Repository\SpecialtyRepository;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use App\Service\ScolariteService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
         if ($this->getUser()) {
             dd($authenticationUtils);
             return $this->redirectToRoute('account');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
    /**
     * @Route("/scolarite/login", name="scolarite_login")
     */
    public function loginFromScolarite(
        LoginFormAuthenticator $authenticator,
        GuardAuthenticatorHandler $guardAuthenticatorHandler,
        Request $request,
        UserRepository $userRepository,
        ScolariteService $scolariteService,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $userPasswordEncoder,
        LevelRepository $levelRepository,
        SpecialtyRepository $specialtyRepository,
        RegistrationRepository $registrationRepository
    )
    {
        $username = $request->get("username");
        $user = $userRepository->findOneBy(["email" => $username]);

        if (is_null($user)) {
            $params = ["email" => $username];
            $result = $scolariteService->getStudentRegistrationByEmail($params);
            $password = $request->get('password');
            if ($result["success"]) {
                $result = $result["etudiants"];
                $user = new User();
                $user->setFirstName($result[0]['prenoms'])
                    ->setLastName($result[0]["nom"])
                    ->setEmail($result[0]['email_uvci'])
                    ->setProfile('ETUDIANT')
                    ->setPassword($userPasswordEncoder->encodePassword($user, $password));
                $entityManager->persist($user);

                foreach ($result as $registration)
                {
                    $level = $levelRepository->findOneBy(["wording" => $registration["niveau_etude"]]);
                    $specialty = $specialtyRepository->findOneBy(["coded" => $registration["code_specialite"]]);

                    if (!is_null($level) && !is_null($specialty))
                    {
                        $inscription = $registrationRepository->findOneBy([
                            "level" => $level,
                            "specialty" => $specialty,
                            "semestre" => $registration["semestre"],
                            "rentree" => $registration["rentree"],
                        ]);

                        // Si l'inscription existe déjà
                        if (is_null($inscription))
                        {
                            $inscription = new Registration();
                            $inscription->setUser($user);
                            $inscription->setLevel($level);
                            $inscription->setSpecialty($specialty);
                            $inscription->setRentree($registration["rentree"]);
                            $inscription->setSemestre($registration["semestre"]);

                            $entityManager->persist($inscription);
                        }
                    }
                }
                $entityManager->flush();
            }
        }

        return $guardAuthenticatorHandler->authenticateUserAndHandleSuccess(
            $user,
            $request,
            $authenticator,
            'main'
        );
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        //throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

}
