<?php

namespace App\Controller\Admin;

use App\Form\QuizManagerType;
use App\Repository\MatterSpecialtyRepository;
use App\Repository\RegistrationRepository;
use App\Service\MatterSpecialityService;
use App\Service\ScolariteService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuizManagerController extends AbstractController
{
    /**
     * @Route("/admin/quiz/manager", name="quiz_manager")
     */
    public function index(
        Request $request, ScolariteService $scolariteService,
        RegistrationRepository $registrationRepository,
        MatterSpecialityService $matterSpecialityService,
        EntityManagerInterface $entityManager,
        MatterSpecialtyRepository $matterSpecialtyRepository
    ): Response
    {
        $years = $scolariteService->getAcademyYears();
        $entries = $scolariteService->getAcademyEntries();
        $form = $this->createForm(QuizManagerType::class, null, [
            "academyYears" => $years,
            "academyEntries" => $entries,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $academyYear = $form->get('academyYear')->getData();
            $academyEntry = $form->get('academyEntry')->getData();
            $level = $form->get('level')->getData();
            $semester = $form->get('semester')->getData();
            $active = $form->get('active')->getData();
            $specialities = $form->get('specialities')->getData();
            $specialities = $specialities->toArray();
            $registrations = $registrationRepository->findBy([
                "specialty" => $specialities,
                "semestre" => $semester,
                "rentree" => $academyEntry,
                "level" => $level,
            ]);

            $matterSpecialityService->activeClaimOnQuiz(
                $specialities, $level, $semester, $academyEntry,
                $active, $matterSpecialtyRepository, $entityManager
            );
            $message = $active ? "Activation " : "Désactivation " . "des réclamations sur les devoirs avec succès!";
            $this->addFlash(
                'success',
                $message
            );
        }
        return $this->render('admin/quiz_manager/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
