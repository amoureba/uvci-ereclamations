<?php

namespace App\Controller\Admin;

use App\Entity\Exam;
use App\Entity\Examination;
use App\Form\Admin\ExamType;
use App\Form\ExamResultReportingType;
use App\Repository\ExamRepository;
use App\Repository\RegistrationRepository;
use App\Service\ExamService;
use App\Service\ScolariteService;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ExaminationRepository;
use App\Repository\MatterSpecialtyRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ExamController extends AbstractController
{
    /**
     * Afficher toutes les sessions
     * @Route("/admin/examens-toutes-les-sessions/index", name="exams_index")
     */
    public function index(ExamRepository $examRepository): Response
    {
        $exams = $examRepository->findAll();
        return $this->render('admin/exam/index.html.twig', [
            'exams' => $exams,
        ]);
    }

    /**
     * Ajouter une session pour (niveau, spécialité, semestre)
     * @Route("/admin/examens/ajouter-des-sessions", name="add_exams")
     * @Route("/admin/modifier/examen/{slug}", name="edit_exam")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param MatterSpecialtyRepository $matterSpecialtyRepository
     * @param ExaminationRepository $examinationRepository
     * @param ExamService $examService
     * @param SluggerInterface $slugger
     * @param ScolariteService $scolariteService
     * @param Exam|null $exam
     * @return Response
     */
    public function add(Request $request,
                        EntityManagerInterface $entityManager,
                        MatterSpecialtyRepository $matterSpecialtyRepository,
                        ExaminationRepository $examinationRepository,
                        ExamService $examService,
                        SluggerInterface $slugger, ScolariteService $scolariteService,
                        Exam $exam = null
    ): Response
    {
        $years = $scolariteService->getAcademyYears();
        $entries = $scolariteService->getAcademyEntries();

        if (is_null($exam))
            $exam = new Exam();
        $form = $this->createForm(ExamType::class, $exam, [
            "academyYears" => $years,
            "academyEntries" => $entries,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $specialties = $form->get('specialities')->getData();
            $level = $form->get('level')->getData();
            $semester = $form->get('semester')->getData();
            $academyEntry = $form->get("academyEntry")->getData();
            $session = $form->get('session')->getData();
            $examService->add(
                $session, $academyEntry, $level, $semester, $exam, $specialties, $matterSpecialtyRepository,
                $slugger, $entityManager, $examinationRepository
            );
            $this->addFlash(
                'success',
                "L'examen a été ajouté avec succès !"
            );
            return $this->redirectToRoute('add_exams');
        }
        return $this->render('admin/exam/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Archiver / Déarchiver une session d'examen
     * @Route("/admin/examens/archiver-desarchiver-la-session/{id}", name="update_exams")
     * @param EntityManagerInterface $entityManager
     * @param Exam $exam
     * @param ExamRepository $examRepo
     * @return Response
     */
    public function update(EntityManagerInterface $entityManager,
    Exam $exam, ExamRepository $examRepo): Response
    {
        if ($examRepo->findOneBy(['id' => $exam])) {
            if($exam->getArchived()){
                $exam->setArchived(false);
                $entityManager->persist($exam);
                // Todo: Uncomment below code to unarchive the claims when the exam is unarchive
                /*$examinations = $exam->getExaminations();
                foreach ($examinations as $examination) {
                    $claims = $examination->getClaims();
                    foreach ($claims as $claim) {
                        $claim->setArchived(false);
                        $entityManager->persist($claim);
                    }
                }*/
            }else{
                $exam->setArchived(true);
                $entityManager->persist($exam);
                $examinations = $exam->getExaminations();
                foreach ($examinations as $examination) {
                    $claims = $examination->getClaims();
                    foreach ($claims as $claim) {
                        $claim->setArchived(true);
                        $entityManager->persist($claim);
                    }
                }
            }
            $entityManager->flush();
            $this->addFlash(
                'success',
                "L'opération a été éffectuée avec succès !"
            );
        } else {
            $this->addFlash(
                'warning',
                "Cet examen n'existe pas dans la base de données !"
            );
        }
        return $this->redirectToRoute('exams_index');
        /*
        $form = $this->createForm(ExamUpdateType::class, $exam);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if($form->get('archived')->getData()){
                $exam->setArchived(true);
                $entityManager->persist($exam);
                $entityManager->flush();
            }else{
                $exam->setArchived(false);
                $entityManager->persist($exam);
                $entityManager->flush();
            }
            $this->addFlash(
                'success',
                "L'examen a été modifié avec succès !"
            );
            return $this->redirectToRoute('exams_index');
        }
        return $this->render('admin/exam/update.html.twig', [
            'form' => $form->createView()
        ]);
        */
    }

    /**
     * Suppression d'une session d'examen par son id
     * @Route("/admin/examens/supprimer-la-session/{id}", name="delete_exam")
     */
    public function delete(
        ExamRepository $examRepo, Exam $exam,
        EntityManagerInterface $entityManager
    ): Response
    {
        if($examRepo->findOneBy(['id' => $exam])){
            $entityManager->remove($exam);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "La suppression a été éffectuée avec succès !"
            );
        } else {
            $this->addFlash(
                'warning',
                "Cet examen n'existe pas dans la base de données !"
            );
        }
        return $this->redirectToRoute('exams_index');
    }

    /**
     * Publier les résultats finaux
     * @Route("/admin/examen/{slug}/result_reporting", name="exam_result_reporting")
     */
    public function exam_result_reporting(
        Request $request,
        RegistrationRepository $registrationRepository, Exam $exam,
        ExamService $examService, MailerInterface $mailer,
        EntityManagerInterface $entityManager
    )
    {
        $form = $this->createForm(ExamResultReportingType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $notabene = $form->get("notabene")->getData();
            $session = $exam->getSession();
            $academyYear = $exam->getAcademyYear();
            $academyEntry = $exam->getAcademyEntry();
            $semester = $exam->getSemester();
            $startDate = $exam->getStartDate();
            $endDate = $exam->getEndDate();
            $specialities = $exam->getSpecialities();
            $level = $exam->getLevel();
            $specialitiesWording = $examService->getSpecialitiesWording($specialities);
            $emailParams = [
                'session' => $session,
                'academyYear' => $academyYear,
                'academyEntry' => $academyEntry,
                'semester' => $semester,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'specialities' => $specialities,
                'level' => $level,
                'notabene' => $notabene,
                'specialitiesWording' => $specialitiesWording
            ];

            if ($form->get("preview")->isClicked()) {
                return $this->render("emails/notif_exam_results_reporting.html.twig", $emailParams);
            }

            $from = "scolarite@uvci.edu.ci";
            $to = [];
            $subject = "[Résultat après réclamation] - Examen Session $session - Rentrée de $academyEntry – Spécialités $specialitiesWording";
            $registrations = $registrationRepository->findBy([
                "level" => $level,
                "specialty" => $specialities->toArray(),
                "semestre" => $semester,
                "rentree" => $academyEntry,
            ]);
            foreach ($registrations as $registration) {
                $to[] = $registration->getUser()->getEmail();
            }
            $email = (new TemplatedEmail())
                ->from(new Address($from, "Scolarité"))
                ->to(...$to)
                ->cc("pef@uvci.edu.ci", "pedagogie@uvci.edu.ci", "examen@uvci.edu.ci",
                    "servicecommunication@uvci.edu.ci", "scsc@uvci.edu.ci")
                ->subject($subject)
                ->htmlTemplate('emails/notif_exam_results_reporting.html.twig')
                ->context($emailParams);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
            }

            // Archiver l'examen
            $exam->setArchived(true);
            $entityManager->persist($exam);

            $entityManager->flush();

            return $this->redirectToRoute("exams_index");
        }

        return $this->render("admin/exam/result_reporting.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * Afficher les détails d'une session d'examen
     * @Route("/admin/examen/{slug}/details", name="details_exam")
     */
    public function details(ExaminationRepository $examinationRepo, Exam $exam): Response
    {
        $examinations = $examinationRepo->findByExam($exam);
        return $this->render('admin/exam/details.html.twig', [
            'exam' => $exam,
            'examinations' => $examinations,
        ]);
    }

    /**
     * Liste des reclamation d'une session d'examen
     * @Route("/admin/examens/reclamation/{id}", name="list_exam_claims")
     * @param Examination $examination
     * @return Response
     */
    public function list_exam_claims(Examination $examination): Response
    {
        $claims = $examination->getClaims();

        return $this->render('admin/exam/claims.html.twig', [
            'claims' => $claims,
            'examination' => $examination,
        ]);
    }

    /**
     * Afficher les détails d'une session d'examen
     * @Route("/admin/examen/{slug}/specialities/exam", name="list_specialities_exam")
     */
    public function list_specialities_exam(Exam $exam): Response
    {
        $specialities = $exam->getSpecialities();
        return $this->render('admin/exam/list_specialities_exam.html.twig', [
            'exam' => $exam,
            'specialties' => $specialities,
        ]);
    }

    /**
     * Suppression d'une ligne de composition par son id
     * @Route("/admin/examens/supprimer-la-composition/{id}", name="delete_exams_examinations")
     */
    public function deleteExamination(ExaminationRepository $examinationRepo, Examination $examination, EntityManagerInterface $entityManager): Response
    {
        if ($examinationRepo->findOneBy(['id' => $examination])) {
            $entityManager->remove($examination);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "La suppression a été éffectuée avec succès !"
            );
        } else {
            $this->addFlash(
                'warning',
                "La ressource demandée n'existe pas dans la base de données !"
            );
        }
        return $this->redirectToRoute('details_exam');
    }
}
