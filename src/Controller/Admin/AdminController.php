<?php

namespace App\Controller\Admin;

use App\Entity\Claim;
use App\Form\SearchClaimsType;
use App\Repository\ClaimRepository;
use App\Repository\ExaminationRepository;
use App\Repository\ExamRepository;
use App\Repository\MatterSpecialtyRepository;
use App\Repository\UserRepository;
use App\Service\StatsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * Tableau de bord administrateur
     * @Route("/admin/tableau-de-bord", name="admin")
     */
    public function index(
        ClaimRepository $claimRepository, StatsService $stats, Request $request,
        UserRepository $userRepository, ExaminationRepository $examinationRepository,
        ExamRepository $examRepository, MatterSpecialtyRepository $matterSpecialtyRepository
    ): Response
    {
        /** BEGIN STATS */
        $statistics = $stats->getStats();
        /** END STATS */

        $examsClaims = null;
        $examsClaimsArchived = $claimRepository->findBy([
            'category' => 'EXAMEN',
            'archived' => true,
        ]);
        $devClaims = $claimRepository->findBy(['category' => 'DEVOIR']);
        $others = $claimRepository->findBy(['category' => 'CONTENU DE COURS']);
        /*$techClaims = $claimRepository->findBy(['category' => 'TECHNIQUE']);*/

        $academyEntriesJson = file_get_contents("https://scolarite.uvci.edu.ci/api/get_rentree");
        $academyEntries = json_decode($academyEntriesJson, true);
        $academyEntries = $academyEntries["rentrees"];
        $rentries = [];
        foreach ($academyEntries as $academyEntry)
        {
            $rentries[$academyEntry["rentree"]] = $academyEntry["code_rentree"];
        }
        $form = $this->createForm(SearchClaimsType::class, null, ["academyEntries" => $rentries]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $session = $form->get('session')->getData();
            $academyEntry = $form->get('academyEntry')->getData();
            $level = $form->get('level')->getData();
            $semester = $form->get('semester')->getData();
            $startDate = $form->get('startDate')->getData();
            $endDate = $form->get('endDate')->getData();
            $speciality = $form->get('speciality')->getData();

            $criteria = [];
            // todo: Comment the above line and uncomment the line below for more search accurate
            /*$criteria = [
                'expire' => false,
                'archived' => false,
            ];*/
            if ($session) $criteria['session'] = $session;
            if ($academyEntry) {
                $criteria['academyEntry'] = $academyEntry;
                $matterSpecialityCriteria['codeRentree'] = $academyEntry;
            }
            if ($level) {
                $criteria['level'] = $level;
                $matterSpecialityCriteria['niveau'] = $level->getWording();
            }
            if ($semester) {
                $criteria['semester'] = $semester;
                $matterSpecialityCriteria['semester'] = $semester;
            }
            if ($startDate) $criteria['startDate'] = $startDate;
            if ($endDate) $criteria['endDate'] = $endDate;
            $exams = $examRepository->findBy($criteria);
            if ($speciality) {
                $matterSpecialityCriteria['codeSpecialite'] = $speciality->getCoded();
                $matterSpeciality = $matterSpecialtyRepository->findBy($matterSpecialityCriteria);
                $examinationCriteria['matterSpecialty'] = $matterSpeciality;
            }
            $examinationCriteria['exam'] = $exams;
            $examinations = $examinationRepository->findBy($examinationCriteria);
            $claimCriteria = [
                'category' => 'EXAMEN',
                'archived' => false,
                'accepted' => false,
                'treated' => false,
            ];
            if ($email) {
                $author = $userRepository->findOneBy(['email' => $email, 'profile' => "ETUDIANT"]);
                $claimCriteria['author'] = $author;
            }
                $claimCriteria['examination'] = $examinations;
                $examsClaims = $claimRepository->findBy($claimCriteria);
        }

        return $this->render('admin/index.html.twig', [
            'statistics' => $statistics,
            'exams_claims' => $examsClaims,
            'dev_claims' => $devClaims,
            'others' => $others,
            'examsClaimsArchived' => $examsClaimsArchived,
            'form' => $form->createView(),
            /*'tech_claims' => $techClaims*/
        ]);
    }

    /**
     * Archiver ou désarchiver une réclamation
     * @Route("/admin/exam/claim/{claim}/archive_dearchive", name="archive_dearchive")
     */
    public function archive_desarchive(Claim $claim, EntityManagerInterface $manager): Response
    {
        $claim->setArchived(!$claim->getArchived());
        $manager->persist($claim);
        $manager->flush();

        return $this->redirectToRoute("admin");
    }
}
