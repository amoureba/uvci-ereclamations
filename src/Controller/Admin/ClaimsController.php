<?php

namespace App\Controller\Admin;

use App\Entity\Claim;
use App\Entity\Answer;
use App\Entity\User;
use App\Form\AnswerType;
use App\Entity\Evaluation;
use App\Entity\Examination;
use App\Form\ClaimAcceptedType;
use App\Form\ClaimTypeType;
use App\Form\SearchClaimsType;
use App\Repository\ClaimRepository;
use App\Repository\ExaminationRepository;
use App\Repository\ExamRepository;
use App\Repository\MatterSpecialtyRepository;
use App\Repository\UserRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Mime\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ClaimsController extends AbstractController
{
    /**
     * Suppression de réclamations depuis le tableau de bord
     * @Route("/admin/reclamation/{id}/supprimer", name="admin_deleteclaims")
     * @return Response
     */
    public function delete(Claim $claim, ClaimRepository $claimRepo, EntityManagerInterface $entityManager)
    {
        if ($claimRepo->findOneBy(['id' => $claim])) {
            $entityManager->remove($claim);
            $entityManager->flush();
            $this->addFlash(
                'success',
                "La suppression a été éffectuée avec succès !"
            );
        } else {
            $this->addFlash(
                'warning',
                "Cette réclamation n'existe pas !"
            );
        }
        return $this->redirectToRoute('admin');
    }

    /**
     * Afficher une réclamation (catégorie : autre) et permettre l'ajout de reponse
     * @Route("admin/reclamation-autre-categorie/{id}", name="admin_others_claims")
     */
    public function other(ClaimRepository$claimRepository, Claim $claim, Request $req, 
    MailerInterface $mailer, EntityManagerInterface $em): Response
    {
        $otherClaim = $claimRepository->findOneBy(['id' => $claim]);
        $answer = new Answer();
        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $answer->setAuthor($this->getUser())
                   ->setClaim($claim);
            $em->persist($answer);
            $em->flush();
            /* DEBUT ENVOI MAIL */
            $from = $this->getUser()->getEmail();
            $fromName = $this->getUser()->getFullName();
            $to = $claim->getAuthor()->getEmail();
            /*$claimContent = $claim->getContent();*/
            $claimWording = $claim->getWording();
            $claimCreatedAt = $claim->getCreatedAt();
            $answerContent = $answer->getContent();
            $subject = 'Une nouvelle Réponse !';
            $email = (new TemplatedEmail())
                ->from(new Address($from, $fromName))
                ->to($to)
                ->subject($subject)
                ->htmlTemplate('emails/notif_answer_other_claim.html.twig')
                ->context([
                    'claim_wording' => $claimWording,
                    'claim_created_at' => $claimCreatedAt,
                    'answer_content' => $answerContent,
                ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
            }
            /* FIN ENVOI MAIL */
            $this->addFlash(
                'success',
                "Votre réponse a été ajoutée avec succès !"
            );
            return $this->redirectToRoute('admin_others_claims', ['id' => $claim->getId()]);
        }
        return $this->render('admin/claims/others.html.twig', [
            'other_claim' => $otherClaim,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Afficher une réclamation (catégorie devoir) et permettre l'ajout de reponse
     * @Route("admin/devoir/reclamation/{claim_id}", name="admin_evaluations_claims")
     * @ParamConverter("claim", options={"mapping": {"claim_id": "id"}})
     */
    public function evaluationClaims(MailerInterface $mailer, ClaimRepository $claimRepository, Claim $claim, Request $req, EntityManagerInterface $em): Response
    {
        $answer = new Answer();
        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $answer->setAuthor($this->getUser())
                   ->setClaim($claim);
            $em->persist($answer);
            $em->flush();
            /* DEBUT ENVOI MAIL */
            $from = $this->getUser()->getEmail();
            $fromName = $this->getUser()->getFullName();
            $to = $claim->getAuthor()->getEmail();
            /*$claimContent = $claim->getContent();*/
            $claimWording = $claim->getWording();
            $claimCreatedAt = $claim->getCreatedAt();
            /*$evaluationWording = $claim->getEvaluation()->getWording();
            $evaluationMatter = $claim->getEvaluation()->getMatter()->getWording();
            $evaluationSemester = $claim->getEvaluation()->getSemester()->getSlug();*/
            $evaluationWording = $claim->getEvaluation();
            $evaluationMatter = $claim->getMatterSpeciality()->getMatiere();
            $evaluationSemester = $claim->getMatterSpeciality()->getSemester();
            $answerContent = $answer->getContent();
            $subject = 'Une nouvelle Réponse !';
            $email = (new TemplatedEmail())
                ->from(new Address($from, $fromName))
                ->to($to)
                ->subject($subject)
                ->htmlTemplate('emails/notif_answer_eva_claim.html.twig')
                ->context([
                    'claim_wording' => $claimWording,
                    'claim_created_at' => $claimCreatedAt,
                    'evaluation_wording' => $evaluationWording,
                    'evaluation_matter' => $evaluationMatter,
                    'evaluation_semester' => $evaluationSemester,
                    'answer_content' => $answerContent,
                ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
            }
            /* FIN ENVOI MAIL */
            $this->addFlash(
                'success',
                "Votre réponse a été ajoutée avec succès !"
            );
            return $this->redirectToRoute('admin_evaluations_claims', ['claim_id' => $claim->getId()]);
        }
        return $this->render('admin/claims/evaluations.html.twig', [
            'evaluation_claim' => $claim,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Afficher la liste des réclamation acceptées
     * @Route("admin/examen/reclamation/acceptees", name="admin_examination_claims_accepted")
     */
    public function acceptedClaims(ClaimRepository $claimRepository,
                                   Request $request, UserRepository $userRepository,
                                   ExamRepository $examRepository,
                                   ExaminationRepository $examinationRepository,
                                   MatterSpecialtyRepository $matterSpecialtyRepository): Response
    {
        $academyEntriesJson = file_get_contents("https://scolarite.uvci.edu.ci/api/get_rentree");
        $academyEntries = json_decode($academyEntriesJson, true);
        $academyEntries = $academyEntries["rentrees"];
        $rentries = [];
        foreach ($academyEntries as $academyEntry)
        {
            $rentries[$academyEntry["rentree"]] = $academyEntry["code_rentree"];
        }
        $examsClaims = null;
        $form = $this->createForm(SearchClaimsType::class, null, ["academyEntries" => $rentries]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $session = $form->get('session')->getData();
            $level = $form->get('level')->getData();
            $academyEntry = $form->get('academyEntry')->getData();
            $semester = $form->get('semester')->getData();
            $startDate = $form->get('startDate')->getData();
            $endDate = $form->get('endDate')->getData();
            $speciality = $form->get('speciality')->getData();

            $criteria = [];
            // todo: Comment the above line and uncomment the line below for more search accurate
            /*$criteria = [
                'expire' => false,
                'archived' => false,
            ];*/
            if ($session) $criteria['session'] = $session;
            if ($academyEntry) {
                $criteria['academyEntry'] = $academyEntry;
                $matterSpecialityCriteria['codeRentree'] = $academyEntry;
            }
            if ($level) {
                $criteria['level'] = $level;
                $matterSpecialityCriteria['niveau'] = $level->getWording();
            }
            if ($semester) {
                $criteria['semester'] = $semester;
                $matterSpecialityCriteria['semester'] = $semester;
            }
            if ($startDate) $criteria['startDate'] = $startDate;
            if ($endDate) $criteria['endDate'] = $endDate;
            $exams = $examRepository->findBy($criteria);

            if ($speciality) {
                $matterSpecialityCriteria['codeSpecialite'] = $speciality->getCoded();
                $matterSpeciality = $matterSpecialtyRepository->findBy($matterSpecialityCriteria);
                $examinationCriteria['matterSpecialty'] = $matterSpeciality;
            }
            $examinationCriteria['exam'] = $exams;
            $examinations = $examinationRepository->findBy($examinationCriteria);
            $claimCriteria = [
                'category' => 'EXAMEN',
                'archived' => false,
                'accepted' => true,
                'treated' => false,
            ];
            if ($email) {
                $author = $userRepository->findOneBy(['email' => $email, 'profile' => "ETUDIANT"]);
                $claimCriteria['author'] = $author;
            }
            $claimCriteria['examination'] = $examinations;
            $examsClaims = $claimRepository->findBy($claimCriteria);
        }

        return $this->render("admin/claims/accepted.html.twig", [
            'claims' => $examsClaims,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Afficher une réclamation (catégorie examen) et permettre l'ajout de reponse
     * @Route("admin/examen/{id}/reclamation/{claim_id}", name="admin_examination_claims")
     * @ParamConverter("claim", options={"mapping": {"claim_id": "id"}})
     */
    public function exam(ClaimRepository $claimRepository, Claim $claim, Examination $examination,
    Request $req, EntityManagerInterface $em, MailerInterface $mailer): Response
    {
        $academyEntry = $examination->getMatterSpecialty()->getCodeRentree();
        $semester = $examination->getMatterSpecialty()->getSemester();
        $level = $examination->getMatterSpecialty()->getNiveau();
        $session = $examination->getExam()->getSession();
        $code_ecue = $examination->getMatterSpecialty()->getCodeEcue();
        $claim_email = $claim->getAuthor()->getEmail();
        $result_json = file_get_contents("https://licence".strtolower($semester).".uvci.edu.ci/api_deliberation/reclamation/get_moyenne_one/$academyEntry/$semester/$level/$session/$code_ecue/$claim_email");
        $result = json_decode($result_json, true);
        $tests_json = file_get_contents("https://licence".strtolower($semester).".uvci.edu.ci/api_deliberation/reclamation/get_mark_student/$academyEntry/$semester/$level/$code_ecue/$claim_email");
        $tests = json_decode($tests_json, true);
        $answer = new Answer();
        $form = $this->createForm(AnswerType::class, $answer);
        $examinationMatter = $examination->getMatterSpecialty()->getMatiere();
        $examSemester = $examination->getExam()->getSemester();
        $examSession = $examination->getExam()->getSession();
        $from = $this->getUser()->getEmail();
        $fromName = $this->getUser()->getFullName();
        $to = $claim->getAuthor()->getEmail();
        $student_name = $claim->getAuthor()->getFullName();
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $answer->setAuthor($this->getUser())
                   ->setClaim($claim);
            $em->persist($answer);
            $em->flush();
            /* DEBUT ENVOI MAIL */
            $claimWording = $claim->getWording();
            $claimCreatedAt = $claim->getCreatedAt();
            $answerContent = $answer->getContent();
            $subject = 'Une nouvelle Réponse !';
            $email = (new TemplatedEmail())
                ->from(new Address($from, $fromName))
                ->to($to)
                ->subject($subject)
                ->htmlTemplate('emails/notif_answer_exam_claim.html.twig')
                ->context([
                    'claim_wording' => $claimWording,
                    'claim_created_at' => $claimCreatedAt,
                    'examination_matter' => $examinationMatter,
                    'exam_semester' => $examSemester,
                    'exam_session' => $examSession,
                    'answer_content' => $answerContent,
                    'student_name' => $student_name,
                ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
            }
            /* FIN ENVOI MAIL */
            $this->addFlash(
                'success',
                "Votre réponse a été ajoutée avec succès !"
            );
            return $this->redirectToRoute('admin_examination_claims', ['id' => $examination->getId(), 'claim_id' => $claim->getId()]);
        }
        $accepted_form = $this->createForm(ClaimAcceptedType::class);
        $accepted_form->handleRequest($req);
        if ($accepted_form->isSubmitted() && $accepted_form->isValid()) {
            $action = $accepted_form->get('motif')->getData();
            $exam = $claim->getExamination()->getExam();
            $claim->setMotif($action);
            $claim->setAccepted(true);
            $claim->setAcceptedBy($this->getUser());
            $claim->setAcademyEntry($examination->getMatterSpecialty()->getCodeRentree());
            $claim->setSemester($examination->getMatterSpecialty()->getSemester());
            $claim->setEcue($examination->getMatterSpecialty()->getCodeEcue());
            $claim->setNiveau($examination->getMatterSpecialty()->getNiveau());
            $claim->setSession($exam->getSession());
            $claim->setEmail($claim->getAuthor()->getEmail());
            $claim->setMotif($accepted_form->get('motif')->getData());
            $em->persist($claim);
            $em->flush();

            $email = (new TemplatedEmail())
                ->from(new Address($from, $fromName))
                ->to($to)
                ->subject("Reclamation encours de traitement")
                ->htmlTemplate('emails/notif_reject_exam_claim.html.twig')
                ->context([
                    'claim_wording' => $claim->getWording(),
                    'claim_created_at' => $claim->getCreatedAt(),
                    'examination_matter' => $examinationMatter,
                    'exam_semester' => $examSemester,
                    'exam_session' => $examSession,
                    'student_name' => $student_name,
                ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
            }
            $this->addFlash(
                'success',
                "Réclamation acceptée avec succès !"
            );
            return $this->redirectToRoute('admin_examination_claims', ['id' => $examination->getId(), 'claim_id' => $claim->getId()]);
        }
        return $this->render('admin/claims/examinations.html.twig', [
            'exam_claim' => $claim,
            'examination' => $examination,
            'form' => $form->createView(),
            'result' => $result,
            'accepted_form' => $accepted_form->createView(),
            'tests' => $tests,
        ]);
    }

    /**
     * Rejeter une reclamation
     * @Route("admin/examen/{id}/reclamation/reject/{claim_id}", name="admin_examination_claim_reject")
     * @ParamConverter("claim", options={"mapping": {"claim_id": "id"}})
     */
    public function reject(Examination $examination,
                           Claim $claim, MailerInterface $mailer, EntityManagerInterface $manager): Response
    {
        $from = $this->getUser()->getEmail();
        $fromName = $this->getUser()->getFullName();
        $to = $claim->getAuthor()->getEmail();
        $student_name = $claim->getAuthor()->getFullName();
        $examinationMatter = $examination->getMatterSpecialty()->getMatiere();
        $examSemester = $examination->getExam()->getSemester();
        $examSession = $examination->getExam()->getSession();
        $claim->setRejected(true)
                ->setRejectedBy($this->getUser())
                ->setArchived(true);
        $manager->persist($claim);
        $manager->flush();

        $email = (new TemplatedEmail())
            ->from(new Address($from, $fromName))
            ->to($to)
            ->subject("Reclamation non-recevable")
            ->htmlTemplate('emails/notif_reject_exam_claim.html.twig')
            ->context([
                'claim_wording' => $claim->getWording(),
                'claim_created_at' => $claim->getCreatedAt(),
                'examination_matter' => $examinationMatter,
                'exam_semester' => $examSemester,
                'exam_session' => $examSession,
                'student_name' => $student_name,
            ]);
        try {
            $mailer->send($email);
        } catch (TransportExceptionInterface $e) {
        }
        $this->addFlash(
            'success',
            "Réclamation réjétée avec succès !"
        );
        return $this->redirectToRoute('admin_examination_claims', ['id' => $examination->getId(), 'claim_id' => $claim->getId()]);

    }
}
