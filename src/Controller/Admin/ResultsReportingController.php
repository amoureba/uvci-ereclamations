<?php

namespace App\Controller\Admin;

use App\Entity\Exam;
use App\Form\ResultsReportingType;
use App\Repository\ExaminationRepository;
use App\Repository\ExamRepository;
use App\Repository\MatterSpecialtyRepository;
use App\Repository\RegistrationRepository;
use App\Service\ExamService;
use App\Service\ScolariteService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class ResultsReportingController extends AbstractController
{
    /**
     * @Route("/admin/results/reporting", name="results_reporting")
     */
    public function index(
        Request $request, ScolariteService $scolariteService,
        MailerInterface $mailer, RegistrationRepository $registrationRepository,
        ExamService $examService, MatterSpecialtyRepository $matterSpecialtyRepository,
        SluggerInterface $slugger, EntityManagerInterface $entityManager,
        ExaminationRepository $examinationRepository,
        ExamRepository $examRepository
    ): Response
    {
        $years = $scolariteService->getAcademyYears();
        $entries = $scolariteService->getAcademyEntries();
        $form = $this->createForm(ResultsReportingType::class, null, [
            "academyYears" => $years,
            "academyEntries" => $entries,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $session = $form->get('session')->getData();
            $academyYear = $form->get('academyYear')->getData();
            $academyEntry = $form->get('academyEntry')->getData();
            $level = $form->get('level')->getData();
            $semester = $form->get('semester')->getData();
            $startDate = $form->get('startDate')->getData();
            $endDate = $form->get('endDate')->getData();
            $specialities = $form->get('specialities')->getData();
            $notabene = $form->get('notabene')->getData();
            $specialities = $specialities->toArray();
            $registrations = $registrationRepository->findBy([
                "specialty" => $specialities,
                "semestre" => $semester,
                "rentree" => $academyEntry,
                "level" => $level,
            ]);
            $specialitiesWording = "";
            $nbSpecialities = count($specialities);
            foreach($specialities as $key => $speciality) {
                $code = $speciality->getCoded();
                $specialitiesWording .= substr($code, 4);
                if(++$key != $nbSpecialities) {
                    $specialitiesWording .= "-";
                }
            }
            $emailParams = [
                'session' => $session,
                'academyYear' => $academyYear,
                'academyEntry' => $academyEntry,
                'semester' => $semester,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'specialities' => $specialities,
                'level' => $level,
                'notabene' => $notabene,
                'specialitiesWording' => $specialitiesWording
            ];
            if ($form->get("preview")->isClicked()) {
                return $this->render("emails/notif_results_reporting.html.twig", $emailParams);
            }
            $session = $session == "normale" ? "SESSION 1" : "SESSION 2";
            $exam = $examRepository->findOneBy([
                "level" => $level,
                "session" => $session,
                "academyEntry" => $academyEntry,
                "semester" => $semester,
            ]);
            $postParams = [
                "rentree" => $academyEntry,
                "niveau" => $level->getWording(),
                "semestre" => $semester,
                "type" => "EXAMEN",
                "valeur" => 1,
            ];
            $result = $scolariteService->results_reporting($postParams);
            if ($exam) {
                $this->addFlash(
                    'warning',
                    "Cette session existe déjà dans la base !"
                );
            } elseif ($result["etat"] == 0) {
                $this->addFlash(
                    'warning',
                    "Nous n'avons pas pu afficher les résultats au niveau de la scolarité. Veuillez réessayer plus tard ou voir le service scolarité. Merci !"
                );
            } else {
                $from = "scolarite@uvci.edu.ci";
                $to = [];
                $subject = "[Résultat Examen] - Session $session - Rentrée de $academyEntry – Spécialités $specialitiesWording";
                foreach ($registrations as $registration) {
                    $to[] = $registration->getUser()->getEmail();
                }
                $email = (new TemplatedEmail())
                    ->from(new Address($from, "Scolarité"))
                    ->to(...$to)
                    ->cc("pef@uvci.edu.ci", "pedagogie@uvci.edu.ci", "examen@uvci.edu.ci",
                        "servicecommunication@uvci.edu.ci", "scsc@uvci.edu.ci")
                    ->subject($subject)
                    ->htmlTemplate('emails/notif_results_reporting.html.twig')
                    ->context($emailParams);
                try {
                    $mailer->send($email);
                } catch (TransportExceptionInterface $e) {
                }
                $exam = new Exam();
                $exam->setLevel($level)
                    ->setSession($session)
                    ->setStartDate($startDate)
                    ->setEndDate($endDate)
                    ->setAcademyYear($academyYear)
                    ->setAcademyEntry($academyEntry)
                    ->setSemester($semester);
                $examService->add(
                    $session, $academyEntry, $level, $semester, $exam, $specialities, $matterSpecialtyRepository,
                    $slugger, $entityManager, $examinationRepository
                );

                $this->addFlash(
                    'success',
                    "Résultats publiés avec succès !"
                );
                return $this->redirectToRoute('results_reporting');
            }
        }

        return $this->render('admin/results_reporting/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
