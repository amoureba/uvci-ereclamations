<?php

namespace App\Service;

use App\Entity\Examination;
use App\Repository\ExaminationRepository;
use App\Repository\MatterSpecialtyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class ExamService
{
    public function add(
        $session, $academyEntry, $level, $semester, $exam, $specialties,
        MatterSpecialtyRepository $matterSpecialtyRepository,
        SluggerInterface $slugger, EntityManagerInterface $entityManager,
        ExaminationRepository $examinationRepository
    )
    {
        $slugExam = $slugger->slug($session . '-' . $academyEntry . '-'.$level->getWording() . '-' . $semester);
        $exam->setSlug($slugExam);
        $exam->setArchived(0);
        $entityManager->persist($exam);//persist exam

        //Ajout des matières de l'examen
        $mattersSpecialties = [];
        foreach ($specialties as $specialty)
        {
            $matters = $matterSpecialtyRepository->findBy(
                [
                    "niveau" => $level->getWording(),
                    "semester" => $semester,
                    'codeSpecialite' => $specialty->getCoded(),
                    'codeRentree' => $academyEntry,
                ]
            );
            $mattersSpecialties[] = $matters;
            $this->deactivateClaimOnQuiz($matters, $entityManager);
        }

        foreach($mattersSpecialties as $matterSpecialities) {
            foreach ($matterSpecialities as $matterSpeciality)
            {
                $examination = $examinationRepository->findOneBy([
                    'matterSpecialty' => $matterSpeciality,
                    'exam' => $exam,
                ]);
                if (is_null($examination))
                    $examination = new Examination();

                $examination->setMatterSpecialty($matterSpeciality);
                $examination->setExam($exam);
                $examination->setSlug($slugger->slug($slugExam. '-'. $matterSpeciality->getMatiere()));
                $entityManager->persist($examination);//persist examination
            }
        }
        $entityManager->flush();
    }

    public function getSpecialitiesWording($specialities): string
    {
        $specialitiesWording = "";
        $nbSpecialities = count($specialities);
        foreach($specialities as $key => $speciality) {
            $code = $speciality->getCoded();
            $specialitiesWording .= substr($code, 4);
            if(++$key != $nbSpecialities) {
                $specialitiesWording .= "-";
            }
        }

        return $specialitiesWording;
    }

    public function deactivateClaimOnQuiz($matters, EntityManagerInterface $entityManager)
    {
        foreach ($matters as $matter)
        {
            $matter->setActiveClaimQuiz(false);
            $entityManager->persist($matter);
        }

        $entityManager->flush();
    }
}