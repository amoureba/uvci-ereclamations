<?php

namespace App\Service;

class ScolariteService
{
    public function getAcademyYears(): array
    {
        $academyYearsJson = file_get_contents("https://scolarite.uvci.edu.ci/api/get_annee");
        $academyYears = json_decode($academyYearsJson, true);
        $academyYears = $academyYears["annees"];
        $years = [];
        foreach ($academyYears as $academyYear)
        {
            $years[$academyYear["annee"]] = $academyYear["annee"];
        }

        return $years;
    }

    public function getAcademyEntries(): array
    {
        $academyEntriesJson = file_get_contents("https://scolarite.uvci.edu.ci/api/get_rentree");
        $academyEntries = json_decode($academyEntriesJson, true);
        $academyEntries = $academyEntries["rentrees"];
        $entries = [];
        foreach ($academyEntries as $academyEntry)
        {
            $entries[$academyEntry["rentree"]] = $academyEntry["code_rentree"];
        }

        return  $entries;
    }

    private function buildContextForPostMethod(array $params)
    {
        $postData = http_build_query([$params]);
        $options = [
            'https' => [
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postData
            ]
        ];

        return stream_context_create($options);
    }

    public function results_reporting(array $params): array
    {
        $context = $this->buildContextForPostMethod($params);
        $result = file_get_contents('https://scolarite.uvci.edu.ci/api/publication', false, $context);

        return json_decode($result);
    }


    public function getStudentRegistrationByEmail(array $params)
    {
        $context = $this->buildContextForPostMethod($params);

        $result = file_get_contents('https://scolarite.uvci.edu.ci/api/get_inscription', false, $context);

        return json_decode($result);
    }
}