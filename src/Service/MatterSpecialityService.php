<?php

namespace App\Service;

use App\Repository\MatterSpecialtyRepository;
use Doctrine\ORM\EntityManagerInterface;

class MatterSpecialityService
{
    public function activeClaimOnQuiz(
        $specialities, $level, $semester, $academyEntry, $active,
        MatterSpecialtyRepository $matterSpecialtyRepository,
        EntityManagerInterface $entityManager
    )
    {
        foreach ($specialities as $speciality)
        {
            $matters = $matterSpecialtyRepository->findBy(
                [
                    "niveau" => $level->getWording(),
                    "semester" => $semester,
                    'codeSpecialite' => $speciality->getCoded(),
                    'codeRentree' => $academyEntry,
                ]
            );

            // active claim on quizes
            foreach ($matters as $matter)
            {
                $matter->setActiveClaimQuiz($active);
                $entityManager->persist($matter);
            }
        }

        $entityManager->flush();
    }
}