<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\MatterSpecialtyRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=MatterSpecialtyRepository::class)
 */
class MatterSpecialty
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $semester;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $niveau;

    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private $matiere;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $anneeUniv;

    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private $codeRentree;

    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private $codeEcue;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $codeMaqEcue;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $codeSpecialite;

    /**
     * @ORM\OneToMany(targetEntity=Claim::class, mappedBy="matterSpeciality")
     */
    private $claims;

    /**
     * @ORM\OneToMany(targetEntity=Examination::class, mappedBy="matterSpecialty")
     */
    private $examinations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activeClaimQuiz = false;

    public function __construct()
    {
        $this->claims = new ArrayCollection();
        $this->examinations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->wording;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSemester(): ?string
    {
        return $this->semester;
    }

    public function setSemester(?string $semester): self
    {
        $this->semester = $semester;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(?string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getMatiere(): ?string
    {
        return $this->matiere;
    }

    public function setMatiere(?string $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getAnneeUniv(): ?string
    {
        return $this->anneeUniv;
    }

    public function setAnneeUniv(?string $anneeUniv): self
    {
        $this->anneeUniv = $anneeUniv;

        return $this;
    }

    public function getCodeRentree(): ?string
    {
        return $this->codeRentree;
    }

    public function setCodeRentree(?string $codeRentree): self
    {
        $this->codeRentree = $codeRentree;

        return $this;
    }

    public function getCodeEcue(): ?string
    {
        return $this->codeEcue;
    }

    public function setCodeEcue(?string $codeEcue): self
    {
        $this->codeEcue = $codeEcue;

        return $this;
    }

    public function getCodeMaqEcue(): ?int
    {
        return $this->codeMaqEcue;
    }

    public function setCodeMaqEcue(?int $codeMaqEcue): self
    {
        $this->codeMaqEcue = $codeMaqEcue;

        return $this;
    }

    public function getCodeSpecialite(): ?string
    {
        return $this->codeSpecialite;
    }

    public function setCodeSpecialite(?string $codeSpecialite): self
    {
        $this->codeSpecialite = $codeSpecialite;

        return $this;
    }

    /**
     * @return Collection|Claim[]
     */
    public function getClaims(): Collection
    {
        return $this->claims;
    }

    public function addClaim(Claim $claim): self
    {
        if (!$this->claims->contains($claim)) {
            $this->claims[] = $claim;
            $claim->setMatterSpeciality($this);
        }

        return $this;
    }

    public function removeClaim(Claim $claim): self
    {
        if ($this->claims->removeElement($claim)) {
            // set the owning side to null (unless already changed)
            if ($claim->getMatterSpeciality() === $this) {
                $claim->setMatterSpeciality(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Examination[]
     */
    public function getExaminations(): Collection
    {
        return $this->examinations;
    }

    public function addExamination(Examination $examination): self
    {
        if (!$this->examinations->contains($examination)) {
            $this->examinations[] = $examination;
            $examination->setMatterSpecialty($this);
        }

        return $this;
    }

    public function removeExamination(Examination $examination): self
    {
        if ($this->examinations->removeElement($examination)) {
            // set the owning side to null (unless already changed)
            if ($examination->getMatterSpecialty() === $this) {
                $examination->setMatterSpecialty(null);
            }
        }

        return $this;
    }

    public function getActiveClaimQuiz(): ?bool
    {
        return $this->activeClaimQuiz;
    }

    public function setActiveClaimQuiz(bool $activeClaimQuiz): self
    {
        $this->activeClaimQuiz = $activeClaimQuiz;

        return $this;
    }
}
