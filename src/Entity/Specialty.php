<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\SpecialtyRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SpecialtyRepository::class)
 * @UniqueEntity(
 *  fields={"coded"},
 *  message="Cette spécialité existe déjà dans la base de données !"
 * )
 */
class Specialty
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $coded;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $wording;

    /**
     * @ORM\OneToMany(targetEntity=Registration::class, mappedBy="specialty", orphanRemoval=true)
     */
    private $registrations;

    /**
     * @ORM\OneToMany(targetEntity=Exam::class, mappedBy="specialty", orphanRemoval=true)
     */
    private $exams;

    /**
     * @ORM\ManyToMany(targetEntity=Exam::class, mappedBy="specialities")
     */
    private $examens;

    public function __toString(): string
    {
        return $this->wording;
    }

    public function __construct()
    {
        $this->registrations = new ArrayCollection();
        $this->exams = new ArrayCollection();
        $this->examens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCoded(): ?string
    {
        return $this->coded;
    }

    public function setCoded(string $coded): self
    {
        $this->coded = $coded;

        return $this;
    }

    public function getWording(): ?string
    {
        return $this->wording;
    }

    public function setWording(string $wording): self
    {
        $this->wording = $wording;

        return $this;
    }

    /**
     * @return Collection|Registration[]
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function addRegistration(Registration $registration): self
    {
        if (!$this->registrations->contains($registration)) {
            $this->registrations[] = $registration;
            $registration->setSpecialty($this);
        }

        return $this;
    }

    public function removeRegistration(Registration $registration): self
    {
        if ($this->registrations->removeElement($registration)) {
            // set the owning side to null (unless already changed)
            if ($registration->getSpecialty() === $this) {
                $registration->setSpecialty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Exam[]
     */
    public function getExams(): Collection
    {
        return $this->exams;
    }

    public function addExam(Exam $exam): self
    {
        if (!$this->exams->contains($exam)) {
            $this->exams[] = $exam;
            $exam->setSpecialty($this);
        }

        return $this;
    }

    public function removeExam(Exam $exam): self
    {
        if ($this->exams->removeElement($exam)) {
            // set the owning side to null (unless already changed)
            if ($exam->getSpecialty() === $this) {
                $exam->setSpecialty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Exam[]
     */
    public function getExamens(): Collection
    {
        return $this->examens;
    }

    public function addExamen(Exam $examen): self
    {
        if (!$this->examens->contains($examen)) {
            $this->examens[] = $examen;
            $examen->addSpeciality($this);
        }

        return $this;
    }

    public function removeExamen(Exam $examen): self
    {
        if ($this->examens->removeElement($examen)) {
            $examen->removeSpeciality($this);
        }

        return $this;
    }
}
