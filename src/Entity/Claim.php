<?php

namespace App\Entity;

use App\Service\UploaderHelper;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ClaimRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=ClaimRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Claim
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $wording;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $capture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity=Answer::class, mappedBy="claim", orphanRemoval=true)
     */
    private $answers;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="claims")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $archived;

    /**
     * @ORM\ManyToOne(targetEntity=Examination::class, inversedBy="claims")
     */
    private $examination;

    /**
     * @ORM\ManyToOne(targetEntity=MatterSpecialty::class, inversedBy="claims")
     */
    private $matterSpeciality;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $evaluation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $rejected = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $accepted = false;

    /**
     * 1 = Modification de note
     * 0 = Autre
     * @ORM\Column(type="integer", nullable=true)
     */
    private $motif;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="acceptedClaims")
     */
    private $acceptedBy;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="rejectedClaims")
     */
    private $RejectedBy;

    /**
     * @ORM\Column(type="boolean")
     */
    private $treated = false;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $academyEntry;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $niveau;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $semester;

    /**
     * @ORM\Column(type="string", length=155, nullable=true)
     */
    private $ecue;

    /**
     * @ORM\Column(type="string", length=155, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $session;

    /**
     * @ORM\PrePersist
     * @return void
     */
    public function prePersist()
    {
        if (empty($this->createdAt)) {
            $this->createdAt = new \DateTime();
        }
    }

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    public function getCapturePath()
    {
        return '/ereclamation/public/uploads/captures/' . $this->getCapture();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getWording(): ?string
    {
        return $this->wording;
    }

    public function setWording(string $wording): self
    {
        $this->wording = $wording;

        return $this;
    }

    public function getCapture(): ?string
    {
        return $this->capture;
    }

    public function setCapture(string $capture): self
    {
        $this->capture = $capture;

        return $this;
    }

    /**
     * @return Collection|Answer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(Answer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setClaim($this);
        }

        return $this;
    }

    public function removeAnswer(Answer $answer): self
    {
        if ($this->answers->removeElement($answer)) {
            // set the owning side to null (unless already changed)
            if ($answer->getClaim() === $this) {
                $answer->setClaim(null);
            }
        }

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the value of category
     */ 
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the value of category
     *
     * @return  self
     */ 
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(?bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getExamination(): ?Examination
    {
        return $this->examination;
    }

    public function setExamination(?Examination $examination): self
    {
        $this->examination = $examination;

        return $this;
    }

    public function getMatterSpeciality(): ?MatterSpecialty
    {
        return $this->matterSpeciality;
    }

    public function setMatterSpeciality(?MatterSpecialty $matterSpeciality): self
    {
        $this->matterSpeciality = $matterSpeciality;

        return $this;
    }

    public function getEvaluation(): ?string
    {
        return $this->evaluation;
    }

    public function setEvaluation(?string $evaluation): self
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    public function getRejected(): ?bool
    {
        return $this->rejected;
    }

    public function setRejected(bool $rejected): self
    {
        $this->rejected = $rejected;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    public function getMotif(): ?int
    {
        return $this->motif;
    }

    public function setMotif(?int $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getAcceptedBy(): ?User
    {
        return $this->acceptedBy;
    }

    public function setAcceptedBy(?User $acceptedBy): self
    {
        $this->acceptedBy = $acceptedBy;

        return $this;
    }

    public function getRejectedBy(): ?User
    {
        return $this->RejectedBy;
    }

    public function setRejectedBy(?User $RejectedBy): self
    {
        $this->RejectedBy = $RejectedBy;

        return $this;
    }

    public function getTreated(): ?bool
    {
        return $this->treated;
    }

    public function setTreated(bool $treated): self
    {
        $this->treated = $treated;

        return $this;
    }

    public function getAcademyEntry(): ?string
    {
        return $this->academyEntry;
    }

    public function setAcademyEntry(?string $academyEntry): self
    {
        $this->academyEntry = $academyEntry;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(?string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getSemester(): ?string
    {
        return $this->semester;
    }

    public function setSemester(?string $semester): self
    {
        $this->semester = $semester;

        return $this;
    }

    public function getEcue(): ?string
    {
        return $this->ecue;
    }

    public function setEcue(?string $ecue): self
    {
        $this->ecue = $ecue;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSession(): ?string
    {
        return $this->session;
    }

    public function setSession(string $session): self
    {
        $this->session = $session;

        return $this;
    }
}
